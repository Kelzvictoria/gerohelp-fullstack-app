import axios from 'axios';
import { GET_REGISTRATIONS, ADD_REGISTRATION, DELETE_REGISTRATION, REGISTRATIONS_LOADING } from './types';


export const getRegistrations = () => dispatch => {
    dispatch(setRegistrationsLoading());

    axios.get('/api/registrations').then(
        (res) => dispatch({
            type: GET_REGISTRATIONS,
            payload: res.data
        })
    )
};

export const deleteRegistration = (id) => dispatch => {
    axios.delete(`/api/registrations/${id}`).then(
        (res) => {
            dispatch({
                type: DELETE_REGISTRATION,
                payload: id
            })
        }
    )
};

export const addRegistration = registration  => dispatch => {
    axios.post('/api/registrations', registration).then(
        (res) => {
            dispatch({
                type: ADD_REGISTRATION,
                payload: res.data
            })
            //console.log('ra', res.data);
        }
    )
};

export const setRegistrationsLoading = () => {
    return {
        type: REGISTRATIONS_LOADING
    }
}