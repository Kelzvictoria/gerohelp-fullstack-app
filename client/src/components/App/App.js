import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { Provider } from 'react-redux';
import store from '../../store';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faPhoneVolume, faEnvelopeOpenText, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import "./App.css";
import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";
import { Home } from "../Home/Home";
import Service from "../Services/Service";
import { About } from "../About/About";
import { Contact } from "../Contact/Contact";
import Register from "../Register/Register"
import Details from "../Staff/Details"
import  List  from "../Staff/List";
import Filter from "../Filter/Filter"

class App extends React.Component {
  render() {
    return (
      <Provider store = {store}>
        <BrowserRouter>
          <div id="container">
            <Header />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/services" component={Service} />
              <Route path="/about" component={About} />
              <Route path="/contact" component={Contact} />
              <Route path="/register" component={Register} />
              <Route path="/staff" component={Details} />
              <Route path="/list" component={List} />
              <Route path="/filter" component={Filter} />
              <Route path="/contact" component={Contact} />
              <Route exact path={"/staff/:id"} component = {Details}/>
              <Route exact path={"/list/:id"} component = {List}/>
            </Switch>
            <Footer />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
library.add(fab, faCheckSquare, faCoffee, faPhoneVolume, faEnvelopeOpenText, faMapMarkerAlt )