import React from 'react'
import {Info} from './Info'
import {Form} from './Form'
import './contact.css'

export const Contact = () => {
    return (
        <div className="contact">
            <div className="row">
                <Info />
                <Form />
            </div>
            
        </div>
    )
}
