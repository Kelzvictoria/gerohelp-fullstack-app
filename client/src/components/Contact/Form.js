import React from 'react'

export const Form = () => {
    return (
        <div className="contact-form">
            <div className="form-container">
                <h3> Make an Appointment</h3>
                <div className="form row">  
                    <div className="col-md-5">
                        <input className="form-control" placeholder="Your name" /><br/>
                        <input className="form-control" placeholder="Your email" /><br/>
                        <input className="form-control" placeholder="Your subject" /><br/>
                    </div>
                    <div className="col-md-7">
                        <textarea className="form-control" placeholder="Your message" />
                    </div>
                </div>
                <br/>
                <div className="last-row row">
                    <div className="col-md-5">
                        <p>All fields are obligatory</p>
                    </div>
                    <div className="col-md-7">
                        <btn class="btn btn-primary">Submit</btn>
                    </div>
                </div>
            </div>
            
        </div>
    )
}