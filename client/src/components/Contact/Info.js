import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const Info = () => {
    return (
        <div className="contact-info">
            <div className="info">
                <p>GeroHelp Info</p>
                <h3>Contact GeroHelp</h3>
                <div className="row">
                    <div className="col-md-4">
                    <FontAwesomeIcon icon="phone-volume"/>&thinsp;Phone
                    <hr />
                    <p>Our Contact Phone</p>
                    <p>+234 90 5523 2411</p>
                    <p>+234 80 6374 1053</p>
                    </div>

                    <div className="col-md-4">
                    <FontAwesomeIcon icon="envelope-open-text"/>&thinsp;Email
                    <hr />
                    <p>Our Contact Email</p>
                    <p>info@gerohelp.com</p>
                    </div>

                    <div className="col-md-4">
                    <FontAwesomeIcon icon="map-marker-alt"/>&thinsp;Address
                    <hr />
                    <p>Our Contact Address</p>
                    <p>20, Holy Cross Street, Ejigbo, Lagos, Nigeria.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}