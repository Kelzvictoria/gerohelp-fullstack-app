import React from 'react'
import { Slider, /*Switch*/ } from 'antd';
import "./filter.css"

class Filter extends React.Component{
    state = {
        disabled: false
    };

    handleDisabledChange = disabled => {
        this.setState({ disabled });
      };

    render(){
        //const { disabled } = this.state;
        return (
            <div className="filter-div">
                    <h3>Filter By?</h3>
                    <Slider defaultValue={30} tooltipVisible />
            </div>
        )
  }
}

export default Filter;