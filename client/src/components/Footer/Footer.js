import React from 'react';
import './footer.css'

export const Footer = () => {
    return (
        <div id="footer">
            <p className="footer-p">copyright - GeroHelp 2020</p>
        </div>
    )
}