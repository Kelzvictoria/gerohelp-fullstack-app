import React from "react";
import "./header.css";
import { HeaderLevelOne } from "./HeaderLevelOne";
import { HeaderLevelTwo } from "./HeaderLevelTwo";
import { HeaderLevelThree } from "./HeaderLevelThree";

export class Header extends React.Component {
  render() {
    return (
      <div id="header">
        <HeaderLevelOne />
        <HeaderLevelTwo />
        <HeaderLevelThree />
      </div>
    );
  }
}
