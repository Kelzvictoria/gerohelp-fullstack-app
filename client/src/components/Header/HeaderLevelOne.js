import React from "react";

export const HeaderLevelOne = () => {
  return (
    <div id="level-one-div" style={{ backgroundColor: "#f6f0d4" }}>
      <p className="level-one-p">getting the best care for your aged ones</p>
    </div>
  );
};
