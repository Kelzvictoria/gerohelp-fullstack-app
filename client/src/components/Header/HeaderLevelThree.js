import React from "react";
import { Link, NavLink } from 'react-router-dom'

export const HeaderLevelThree = () => {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-light bg-light"
      id="level-three-div"
    >
      <Link to = '/' className="navbar-brand">
        GEROHELP
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className=" navbar-nav navbar-right ml-auto">
          <li className="nav-item active">
            <NavLink to = '/' className="nav-link">
              Home <span className="sr-only">(current)</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to = "/services" className="nav-link">
              Services
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to = "/register" className="nav-link">
              Register
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to = "/about" className="nav-link">
              About
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to = "/contact" className="nav-link">
              Contact
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
