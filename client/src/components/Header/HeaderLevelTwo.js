import React from "react";

export const HeaderLevelTwo = () => {
  return (
    <div id="level-two-div" className="row container-fluid">
      <div className="left">
        <p className="cn">Call Now</p>
        <p className="num">
          <i className="fa fa-phone"></i>+234 905 523 2422
        </p>
      </div>
      <div className="right">
        <p>CONTACT US TODAY</p>
      </div>
    </div>
  );
};
