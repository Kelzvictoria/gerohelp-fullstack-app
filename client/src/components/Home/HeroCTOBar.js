import React from 'react'

export const HeroCTOBar = () => {
    return (
        <div id="hero-cto">
            <div id="hero-cto-content"  className="row">
                <p>Give your elderly the love, care and attention they deserve.</p>
                 <a href="/services" className="btn btn-large">ACT NOW</a>
            </div>
            
        </div>
    )
}
