import React from 'react'
import { HeroCTOBar } from './HeroCTOBar'
import './hero.css'

export const Home = () => {
    return (
        <div id="hero">
            <HeroCTOBar />
        </div>
    )
}