import React, { Component } from 'react';
import $ from 'jquery'
//import './previewImage'
import sideimage from "../../assets/sideimage.jpg";
import { connect } from 'react-redux';
import { addRegistration } from '../../actions/registrationActions';

import "./register.css"

class RegisterForm extends Component { 
    
    constructor(props){
        super(props)
        this.state={
            firstName: "",
            lastName: "",
            dob: "",
            address1: "",
            city: "",
            state: "",
            phone: "",
            nok: "",
            isVerified: false,
            qualifications: "",
            maritalStatusId: 0,
            imageUrl: sideimage,
            workExperience: "",
            gender: 0,
            languages: ""
        }
        //this.readImgURL = this.readImgURL.bind(this);
    }

    onChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    onSubmit = e => {
        e.preventDefault();
        const newRegistration = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            dob: this.state.dob,
            address1: this.state.address1,
            city: this.state.city,
            state: this.state.state,
            phone: this.state.phone,
            nok: this.state.nok,
            isVerified: this.state.isVerified,
            qualifications: this.state.qualifications,
            maritalStatusId: this.state.maritalStatusId,
            imageUrl: this.state.imageUrl,
            workExperience: this.state.workExperience,
            gender: this.state.gender,
            languages: this.state.languages
        };
        this.props.addRegistration(newRegistration);
    };

    /*readImgURL(){
        let file = this.refs.file.files[0];
        console.log('file', file);
        let reader = new FileReader();
        let url = reader.readAsDataURL(file);
        reader.onloadend = (e) => {
            this.setState({
                imageUrl: [reader.result]
            });
        }
        console.log(this.state.imageUrl);
    }*/
    
    render() { 
        return ( 
           <div className="register-page row"> 
            <div className="r-form">  
                <div className="container">
                <h3 className="h-text">Work with us</h3>
                    <form onSubmit = {this.onSubmit}>
                        <div className="row">
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="firstName" onChange={this.onChange} placeholder="First Name" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="lastName" onChange={this.onChange} placeholder="Last Name" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <select name="title" onChange={this.onChange} placeholder="Title" className="form-control" />
                            </div>
                            <div className = "col-md-4 selectbox">
                                <select name="category" onChange={this.onChange} placeholder="Job Role" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="dob" onChange={this.onChange} placeholder="Date of Birth" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="qualifications" onChange={this.onChange} placeholder="Highest Qualification" className="form-control" />
                            </div>
                            <div className = "col-md-4 selectbox">
                                <input type="text" name="state" onChange={this.onChange} placeholder="State of Origin" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="address1" onChange={this.onChange} placeholder="Home Address" className="form-control" />
                            </div>
                            <div className = "col-md-4 selectbox">
                                <select  name="city" onChange={this.onChange} placeholder="City" className="form-control" />
                            </div>
                            <div className = "col-md-4 selectbox">
                                <select name="stateOfResidence" onChange={this.onChange} defaultValue="State of residence" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="phone" onChange={this.onChange} placeholder="Phone Number" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="nokName" onChange={this.onChange} placeholder="NOK Name" className="form-control" />
                            </div>
                            <div className = "col-md-4 selectbox">
                                <input type="text" name="nokRelationship" onChange={this.onChange} placeholder="NOK Relationship" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox">
                                <input type="text" name="nokPhone" onChange={this.onChange} placeholder="NOK Phone" className="form-control" />
                            </div>
                            <div className = "col-md-4 txtbox file_upload">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div>
                                    <div className="custom-file">
                                        <input type="file" /*  ref="file" onChange={this.readImgURL} accept="image/*" */ className="custom-file-input" id="inputGroupFile01"
                                        aria-describedby="inputGroupFileAddon01"/>
                                        <label className="custom-file-label">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div className = "col-md-12">
                                <textarea name="about" placeholder="Tell us about yourself" className="form-control" />
                            </div>
                            <div className = "col-md-12 submit-btn">
                                
                                <button className="btn btn-large btn-primary">ACT NOW</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div className="s-image">
            <div className="r-s-image">
                <img id = "ouput_image" src = {this.state.imageUrl} className ="image"/>
            </div>
           </div> 
        </div>
         );
    }
}

const mapStateToProps = state => ({
    registration: state.registration
});
 
export default connect(mapStateToProps, { addRegistration })(RegisterForm);