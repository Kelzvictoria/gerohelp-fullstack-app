import React from 'react';
import sideimage from "../../assets/sideimage.jpg";

export const SideImage = () => {
    return (
        <div className="s-image">
            <div className="r-s-image">
                <img id = "ouput_image" src = {sideimage} className ="image"/>
            </div>
            
        </div>
    )
}