import React, { Component } from "react";
import {Link} from 'react-router-dom'

import "./service.css";
import housekeeping from "../../assets/housekeeping.png"
import clean from "../../assets/clean.png"
import nurses from "../../assets/nurses.png"
import maid from "../../assets/maid.png"
import chef from "../../assets/chef.png"
import security from "../../assets/security.png"
import driver from "../../assets/driver.png"
import './card'

class Service extends Component {
  state = {
    service: [
      {
        _id: 'housekeeping',
        name: "Housekeeping",
        image: housekeeping,
        url: "",
      },
      {
        _id: "cleaning",
        name: "Cleaning",
        image: clean,
        url: "",
      },
      {
        _id: "nurses",
        name: "Nurses",
        image: nurses,
        url: "",
      },
      {
        _id: "maids",
        name: "Maids",
        image: maid,
        url: "",
      },
      {
        _id: "chefs",
        name: "Chefs",
        image: chef,
        url: "",
      },
      {
        _id: "guards",
        name: "Security Guards",
        image: security,
        url: "",
      },
      {
        _id: "drivers",
        name: "Drivers",
        image: driver,
        url: "",
      },
    ],
  };
  render() {
    console.log(this.state);
    return (
      <div className="service-page">
        <div className="services-container row">
        {this.state.service.map((ser, key) => {
          return (
            
               <div className="col-md-3" id="service-card" key={key}>
                  <div className="card">
                    <div className="card-body">
                    <img src= {ser.image}className="card-image"/>
                    <p><Link to = {`/list/${ser._id}`}>{ser.name}</Link></p>
                    </div>
                  </div>
                </div>
            
            
          );
        })}
        </div>
      </div>
    );
  }
}

export default Service;
