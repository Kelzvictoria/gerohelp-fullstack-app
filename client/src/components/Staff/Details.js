import React from 'react'
import axios from 'axios';

import './staff.css'
import nurse from '../../assets/nurse.jpg'

class Details extends React.Component {
    state = {
        firstName: '',
        lastName: '',
        dob: '',
        address1: '',
        city: '',
        state: '',
        phone: '',
        nok: {},
        isVerified: false,
        qualifications: [],
        maritalStatusId: 0,
        imageUrl: '',
        workExperience: [],
        gender: 0,
        languages: [],
        email: "",
        about: ""
    }

    componentDidMount(){
        const url = this.props.location.pathname;
        //console.log(url);

        let split = url.split("/")
        const _id = split[2];
        console.log("_id:",_id);

        axios.get(`http://localhost:5000/api/registrations/${_id}`)
            .then(
                (res) => {
                    console.log(res.data)
                    this.setState({
                        firstName: res.data.firstName,
                        lastName: res.data.lastName,
                        dob: res.data.dob,
                        address1: res.data.address1,
                        city: res.data.city,
                        state: res.data.state,
                        phone: res.data.phone,
                        nok: res.data.nok,
                        isVerified: res.data.isVerified,
                        qualifications: res.data.qualifications,
                        maritalStatusId: res.data.maritalStatusId,
                        imageUrl: res.data.imageUrl,
                        workExperience: res.data.workExperience,
                        gender: res.data.gender,
                        languages: res.data.languages,
                        email: res.data.email,
                        about: res.data.about
                    })
                }
                
            ).catch( (err) => {
                console.log(err);
        })
    }

    render(){
        const { imageUrl,firstName, lastName, qualifications, dob, email, phone, city, state, maritalStatusId, about} = this.state;
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        console.log(yyyy);

        let maritalStatus;

        if(maritalStatusId){
            if(maritalStatusId === 0){
                maritalStatus ="Single"
            } else if (maritalStatusId ===1){
                maritalStatus="Married"
            } else if(maritalStatusId===2){
                maritalStatus="Divorced"
            } else {
                maritalStatus="N/A"
            }
        }

        return (
        <div className="details">
            <div className="row">
            <div className="details-left">
                <div className="profile-img-div">
                    <img src={imageUrl} className="profile-img" alt=""/>
                </div>
                
                <span id="fullname">{firstName} {lastName}</span><br/>
                <p id="role"> {qualifications.map(q =>q)} </p>
                <span>Age: </span> <span>{yyyy - dob.split('-')[0]} years old</span><br/>
        <span>Email: </span> <span>{email}</span><br/>
        <span>Phone: </span> <span>{phone}</span><br/>
                <span>Location: </span> <span>{city}, {state} State</span><br/>
        <span>Marital Status: </span> <span>{maritalStatus}</span>
            </div>
            <div className="details-right">
                <h3>About me</h3>
                <p>{about}</p>
            </div>
            </div>
        </div>
    )
    }   
}

export default Details;