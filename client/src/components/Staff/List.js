import React from 'react';
import { Link, Route } from 'react-router-dom';

import { connect } from 'react-redux';
import { getRegistrations, deleteRegistration } from '../../actions/registrationActions'
import PropTypes from 'prop-types';

import nurse from '../../assets/nurse.jpg'

import { Empty } from 'antd';

 class List extends React.Component{
    
    componentDidMount(){
        this.props.getRegistrations();
    }

    //should be moved to geroHelp Back Office
    onDeleteClick = id => {
        this.props.deleteRegistration(id);
    }

    /*handleGetDetails = id => {
        this.props.getRegistration(id);
    }*/

    render(){
        const url = this.props.location.pathname;
        //console.log(url)
        let split = url.split("/")
        const category = split[2];
        console.log("category:", category);

        const { registrations } = this.props.registration;
        console.log('all registrations',registrations);
        const filteredReg = registrations.filter( r => r.category === category);
        console.log('filtered reg by category', filteredReg);

        return (
            
            <div className="list">
                <div className="row">
                    <div className="list-left">
                        <h3>Our Staff</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur augue turpis, quis sodales dolor fermentum id. Maecenas ultricies, turpis vitae placerat ultrices, velit justo consequat libero, vel venenatis ipsum arcu ut neque. Suspendisse interdum at lorem et blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam semper, ex a luctus sodales, arcu risus eleifend lectus, laoreet pellentesque quam lacus eu sem. Praesent a cursus odio. Vestibulum et varius felis. Sed condimentum arcu eu lacus egestas. </p>
                    </div>
                    <div className="list-right">
                    <a href="#" className="filter-link">Want to filter?</a>
                        <div className="row">
                            {filteredReg.length !== 0 ? 
                                filteredReg.map(reg => {
                                    return (
                                        <div  className="col-md-3 list-items" key={reg._id} style={{backgroundImage:`url(${nurse})`}}>
                                            
                                            <span id="item-fullname" /*onClick={this.handleGetDetails.bind(this, reg._id)}*/>{reg.firstName} {reg.lastName}</span><br/>
                                            <span id="item-role">{reg.qualifications}</span><br/>
                                            <span id="item-email">{reg.email}</span><br/>
                                            <Link to = {`/staff/${reg._id}`}>see more</Link>
                                            {/* <a href="" onClick ={this.onDeleteClick.bind(this, reg._id)}>see more</a> */}
                                        </div>
                                    )
                                }):
                                <Empty className="empty"/>
                            }
                            
                            
                        </div>
                        <nav aria-label="..." className="page-nums">
                        <ul className="pagination">
                            <li className="page-item disabled">
                            <a className="page-link" href="#" tabIndex="-1">Previous</a>
                            </li>
                            <li className="page-item active"><a className="page-link" href="#">1</a></li>
                            <li className="page-item">
                            <a className="page-link" href="#">2 <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item">
                            <a className="page-link" href="#">Next</a>
                            </li>
                        </ul>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
    
};

List.propTypes = {
    getRegistrations: PropTypes.func.isRequired,
    registration: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    registration: state.registration
});

export default connect(mapStateToProps, {getRegistrations, deleteRegistration})(List);