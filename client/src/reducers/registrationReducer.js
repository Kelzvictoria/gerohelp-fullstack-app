import { GET_REGISTRATIONS, GET_REGISTRATION, ADD_REGISTRATION, DELETE_REGISTRATION, REGISTRATIONS_LOADING } from '../actions/types';

const initialState = {
    registrations: [],
    loading: false
};

export default function(state = initialState, action) {
    switch (action.type){
        case GET_REGISTRATIONS:
            return {
                ...state,
                registrations: action.payload,
                loading: false
            }

        case GET_REGISTRATION:
            return {
                ...state,
                registrations: state.registrations.find(reg => reg._id === action.payload)
            }

        case DELETE_REGISTRATION:
            return {
                ...state,
                registrations: state.registrations.filter(reg => reg._id !== action.payload)
            }
        case ADD_REGISTRATION:
            return {
                ...state,
                registrations: [action.payload, ...state.registrations]
            };
        case REGISTRATIONS_LOADING:
            return {
                ...state,
                loading: true
            }

        default:
            return state;
    }
}