const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReqistrationSchema = new Schema({
    firstName: {
        type: String,
        //required: true
    },
    lastName: {
        type: String,
        //required: true
    },
    dob: {
        type: Date
    },
    address1: {
        type: String,
        //required: true
    },
    city: {
        type: String,
        //required: true
    },
    state: {
        type: String,
        //required: true
    },
    phone: {
        type: String,
        //required: true
    },
    nok: {
        type: Object
    },
    isVerified: {
        type: Boolean
    },
    qualifications: {
        type: Array
    },
    maritalStatusId: {
        type: Number
    },
    imageUrl: {
        type: String
    },
    workExperience: {
        type: Array
    },
    gender: {
        type: Number
    },
    languages: {
        type: Array,
        default: ['ENGLISH']
    },
    email: {
        type: String
    },
    about: {
        type: String
    },
    category: {
        type: String
    }
});

module.exports = Registration = mongoose.model('registration', ReqistrationSchema);