const express = require('express');
const router = express.Router();

const Registration = require('../../models/Registration');

// @route GET api/registrations
// @route Get All Registrations
// @access Public
router.get('/', (req, res) => {
    Registration.find().sort({ date: -1 })
    .then( registrations => {
        return res.json(registrations);
    })
});

// @route GET api/registrations/:id
// @route Get A single Registration
// @access Public
router.get('/:id', (req, res) => {
    Registration.findById(req.params.id)
    .then( registration => {
        return res.json(registration);
    })
});

// @route POST api/registrations
// @route Post A Registration
// @access Public
router.post('/', (req, res) => {
    const newRegistration = new Registration({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        dob: req.body.dob,
        address1: req.body.address1,
        city: req.body.city,
        state: req.body.state,
        phone: req.body.phone,
        nok: req.body.nok,
        isVerified: req.body.isVerified,
        qualifications: req.body.qualifications,
        maritalStatusId: req.body.maritalStatusId,
        imageUrl: req.body.imageUrl,
        workExperience: req.body.workExperience,
        gender: req.body.gender,
        languages: req.body.languages,
        email: req.body.email,
        about: req.body.about,
        category: req.body.category
    });

    newRegistration.save().then(
        (registration) => {
            return res.json(registration)
        } 
    );
});

// @route DELETE api/registrations
// @route Delete A Registration
// @access Public
router.delete('/:id', (req, res) => {
    Registration.findById(req.params.id).then(
        (registration) => {
            registration.remove().then(
                () => {
                    return res.json({
                        success: true
                    })
                }
            )
        }
    ).catch(
        (err) => {
            return res.status(404).json({
                success: false
            });
        }
    );
});

module.exports = router;