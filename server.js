const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const registrations = require('./routes/api/registrations');

app.use(bodyParser.json());
app.use(cors());

const db = require('./config/keys').mongoURI;

mongoose.connect(db, { useUnifiedTopology: true, useNewUrlParser: true }).then(
    () => {
        console.log('MongoDB Connected...')
    }
).catch(
    (err) => {
        console.log(err)
    }
);

app.use('/api/registrations', registrations);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server started on port: ${PORT}`);
});